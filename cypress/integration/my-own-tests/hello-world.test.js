/// <reference types="cypress" />

/* describe('Empty Test', () => {
    it('We have correct title', () => {

        cy.viewport('iphone-se2')

        // throw new Error('Oopss');
        cy.visit('https://codedamn.com');
        // cy.contains('Interactively').should('exist');
        cy.contains('Learn Programming');

        //get
        cy.get('div[id=root]').should('exist'); 

        //should
        cy.contains('Learn').should('exist');

    })
}) */

describe('Basic Unauthenticated Desktop Test', ()=> {

    beforeEach(()=>{
        //Boostraping external thing
        cy.viewport(1280, 720)
        cy.visit('http://codedamn.com')
    })

    /* it('The webpage loads, at elast', () => {
        cy.visit('https://codedamn.com')
    }) */

    it.only('Login looks like good', () => {
        /* cy.viewport(1280, 720)
        cy.visit('https://codedamn.com') */

       /*  cy.contains('Sign in to codedamn').should('exist')
        cy.contains('Sign in with Google').should('exist')
        cy.contains('Sign in with GitHub').should('exist')
        cy.contains('Forgot your password?').should('exist') */

        //sign in page
        cy.contains('Sign in').click()
        //password reset page
        cy.log('Going to forgot password')
        cy.contains('Forgot your password?').click()
        //verify your page
        cy.url().should('include', '/password-reset')

        cy.url().then((value) => {
            cy.log('The current real URL is: ', value)
        })

        cy.log('The current URL is', cy.url())
       
        //go back
        cy.go('back')

        cy.contains('Create one').click();
        cy.url().should('include', '/register');

    })

    it('Login should display correct error', () => {
        /* cy.viewport(1280, 720)
        cy.visit('https://codedamn.com') */
        cy.contains('Sign in').click()
        cy.contains('Unable to authorize').should('not.exist')
        cy.get('body').click();
        cy.get('[data-testid="username"]').type('admin', {force: true})
        cy.get('[data-testid="password"]').type('admin', {force: true})
        cy.get('[data-testid="login"]').click();
        cy.contains('Unable to authorize').should('exist')
    })

    it('Login should work fine', () => {
        

        /* cy.viewport(1280, 720)
        cy.visit('https://codedamn.com') */
        cy.contains('Sign in').click()
        cy.get('body').click();
        cy.get('[data-testid="username"]').type('admin', {force: true})
        cy.get('[data-testid="password"]').type('password', {force: true})
        cy.get('[data-testid="login"]').click();
        cy.url().should('include', '/dashboard')
    })
})