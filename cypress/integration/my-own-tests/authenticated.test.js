/// <reference types="cypress" />

describe('Basic Authenticated Desktop Test', ()=> {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiX2lkIjoiNjE1NWIzNjg3ZWRiYmQwMDA5YzQ2YzcyIiwibmFtZSI6ImFkbWluIiwiaWF0IjoxNjYzMjg1MjM0LCJleHAiOjE2Njg0NjkyMzR9.t_ovOe5ZjbvpWqu-glRhzggJLINJGaKspT7Qd1w2ZIg'

    before(()=>{
        cy.then(()=>{
            window.localStorage.setItem('__auth__token', token)
        })
    })

    beforeEach(()=>{
        //Boostraping external thing
        cy.viewport(1280, 720)
        cy.visit('http://codedamn.com')
    })

    it('Should pass', ()=>{})
})